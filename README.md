# docker-kics-scanner

[![pipeline status](https://gitlab.com/dolfen-software/templates/security/docker-kics-scanner/badges/main/pipeline.svg)](https://gitlab.com/dolfen-software/templates/security/docker-kics-scanner/-/commits/main) [![Latest Release](https://gitlab.com/dolfen-software/templates/security/docker-kics-scanner/-/badges/release.svg)](https://gitlab.com/dolfen-software/templates/security/docker-kics-scanner/-/releases)

A simple .gitlab-ci.yml template for including a security scanner job based on the kics docker image.

It supplies two templates:

- The base template, which only creates a hidden job ready to be extended by you.
- The job template, which can be integrated with including it and defining the `test` stage.

## Usage

Include the template like this:

### Base file with anchors

```yaml
include:
  - remote: https://gitlab.com/dolfen-software/templates/security/docker-kics-scanner/-/raw/v1.2.1/scan/base.gitlab-ci.yml
```

### Job file

```yaml
include:
  - remote: https://gitlab.com/dolfen-software/templates/security/docker-kics-scanner/-/raw/v1.2.1/scan/job.gitlab-ci.yml
```

You can modify kics behaviour and more via variables.

Here is a list of the variable names with their respective defaults:

| Variable              | Default                     | Purpose                                                                                                                                                     |
|---------------------- |---------------------------- |------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `IMAGE_TAG`           | v2.1.3-alpine              | Customize the tag of the `checkmarx:kics` image to use. Available tags are listed here: <https://hub.docker.com/r/checkmarx/kics/tags>                        |
| `TYPE`                | Dockerfile                  | The platform type to scan. Takes a case insensitive list of platform types. Example: (Ansible, AzureResourceManager, Bicep, Buildah, CICD, CloudFormation, Crossplane, DockerCompose, Dockerfile, GRPC,GoogleDeploymentManager, Knative, Kubernetes, OpenAPI, Pulumi, ServerLessFW, Terraform)                                                                                                                                  |
| `REPORT_FORMATS`      | json                        | The default reporting format.                                                                                                                               |
| `DEFAULT_OPTS`        | --no-progress --minimal-ui  | Default options to format the output of the scanner.                                                                                                        |
| `PATH`                | $CI_PROJECT_DIR             | Default path to scan for the available platform type.                                                                                                       |
| `OUTPUT_PATH`         | $CI_PROJECT_DIR             | Default path to output the result to.                                                                                                                       |
| `OUTPUT_NAME`         | kics-results                | Default output name.                                                                                                                                        |
| `CONFIG_FILE_PATH`    | ""                          | An optional config file to parse for customization.                                                                                                         |
| `EXCLUDE_TYPE`       | ""                          | A case insensitive list of platform types to exclude from scanning. See `TYPE` for values.   |
| `EXCLUDE_PATHS`       | ""                          | Manages which paths should be excluded during the scan. Default behaviour of kics is scanning a .gitignore file if present and adding them to these paths.  |
| `EXCLUDE_QUERIES`     | ""                          | When running the scanner, some vulnerabilites can be ignored by supplying a comma separated string of the rule ids to ignore.                               |
| `EXCLUDE_RESULTS`     | ""                          | Exclude results by providing the similarity ID of a result                                                                                                  |
| `EXCLUDE_SEVERITIES`  | ""                          | Exclude a severity category by supplying the name of the severity.                                                                                          |

For more customization, please visit <https://docs.kics.io/latest/commands/>.

## Examples

### Extending the base job

```yaml
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH != "main" && $CI_PIPELINE_SOURCE != "merge_request_event"      
      when: never
    - when: always

stages:
  - test

include:
  - remote: https://gitlab.com/dolfen-software/templates/security/docker-kics-scanner/-/raw/v1.2.1/scan/job.gitlab-ci.yml

variables:
  EXCLUDE_QUERIES: 'b03a748a-542d-44f4-bb86-9199ab4fd2d5,fd54f200-402c-4333-a5a4-36ef6709af2f'
```
